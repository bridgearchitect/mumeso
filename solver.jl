include("alltypes.jl")
include("system.jl")
include("linear.jl")

# function to solve physical system numerically using multigrid method
function solveSystemsMultigrid(systemCoars, systemDen, numIterMult, numIterRich, omega)

    # launch multigrid method
    for i in 1:numIterMult
        # apply Richardson relaxation (denser grid)
        solveLinearNum(systemDen.stiffMatrix, systemDen.solVector, systemDen.loadVector, omega, numIterRich)
        # restrict one system into another system
        restrictSystems(systemCoars, systemDen)
        # solve system of linear equations (coarser grid)
        systemCoars.solVector = solveLinearPrec(systemCoars.stiffMatrix, systemCoars.loadVector -
                                systemCoars.stiffMatrix * systemCoars.corrVector)
        # prolongate one system into another system
        prolongateSystems(systemCoars, systemDen)
        # fix error using correction vector
        fixErrorCorrectionVector(systemDen)
        # apply Richardson relaxation (denser grid)
        solveLinearNum(systemDen.stiffMatrix, systemDen.solVector, systemDen.loadVector, omega, numIterRich)
    end

end

# function to solve physical system precisely using Gauss method
function solveSystemsGauss(systemDen)

    # launch Gauss method
    systemDen.solVector = solveLinearPrec(systemDen.stiffMatrix, systemDen.loadVector)

end
