include("alltypes.jl")

# function to create grids
function createGrid(Nx, Ny, ax, bx, ay, by)

    # create grid structure
    grid = Grid()
    # initialize grid structure
    grid.Nx = Nx
    grid.Ny = Ny
    grid.ax = ax
    grid.bx = bx
    grid.ay = ay
    grid.by = by

    # create array of points
    grid.points = Array{Float64, 2}(undef, Nx * Ny, 2)

    # fill array of points
    for j in 1:Ny
        for i in 1:Nx
            grid.points[i + (j - 1) * Nx, 1] = ax + (bx - ax) * (i - 1) / (Nx - 1)
            grid.points[i + (j - 1) * Nx, 2] = ay + (by - ay) * (j - 1) / (Ny - 1)
        end
    end

    # return grid
    return grid

end

# function to find neighbour nodes in grid for the specified point
function findNeighbourNodesInGrid(grid, xcur, ycur)

    # find number of nodes
    Nx = grid.Nx
    Ny = grid.Ny
    numNodes = Nx * Ny
    # initialization
    minDist = inf
    neighIndeces = Array{Int, 1}(undef, 0)

    # go through all points
    for i in 1:numNodes
        # find minimal distance for neighbour nodes
        xp = grid.points[i,1]
        yp = grid.points[i,2]
        dist = calculateL1Norm(xcur, ycur, xp, yp)
        if dist < minDist
            minDist = dist
        end
    end

    # go through all points
    for i in 1:numNodes
        # find neighbour nodes
        xp = grid.points[i,1]
        yp = grid.points[i,2]
        dist = calculateL1Norm(xcur, ycur, xp, yp)
        if abs(dist - minDist) < smallNumber
            append!(neighIndeces, [i])
        end
    end

    # return minimal distance and indexes of neighbour nodes
    return minDist, neighIndeces

end
