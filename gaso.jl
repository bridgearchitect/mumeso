include("alltypes.jl")
include("grid.jl")
include("system.jl")
include("solver.jl")
include("write.jl")

# create grid
gridDens = createGrid(NxDens, NyDens, axDens, bxDens, ayDens, byDens)

# create special objects to describe physical systems
systemDens = createSystem(gridDens, fExt)

# solve physical system using Gauss method
@time begin
    solveSystemsGauss(systemDens)
end

# write solution in text file
writeSolution(systemDens, filenameOutput)
