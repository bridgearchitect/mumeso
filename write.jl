include("alltypes.jl")

# function to write solution in file
function writeSolution(system, filenameOutput)

    # find number of nodes
    Nx = system.grid.Nx
    Ny = system.grid.Ny
    numNodes = Nx * Ny

    # open file
    file = open(filenameOutput, "w")
    # write header row
    write(file, "x y u\n")

    # go through all nodes
    for i in 1:numNodes
        # find physical values
        xcur = system.grid.points[i,1]
        ycur = system.grid.points[i,2]
        ucur = system.solVector[i]
        # print values in file
        write(file, string(xcur) * " " * string(ycur) * " " * string(ucur) * "\n")
    end

    # close file
    close(file)

end
