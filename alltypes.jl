# define default constants
noIndex = -1
zeroNumber = 0.0
inf = 100000000.0
smallNumber = 0.00001

# define grid constants (the denser grid)
axDens = -1.0
bxDens = 1.0
ayDens = -1.0
byDens = 1.0
NxDens = 101
NyDens = 101

# define grid constants (the coarser grid)
axCoars = -1.0
bxCoars = 1.0
ayCoars = -1.0
byCoars = 1.0
NxCoars = 51
NyCoars = 51

# define physical constants
fExt = 1.0

# define constants for mathematical solver
omega = 0.0001
numIterMult = 4
numIterRich = 4

# define string constants
filenameOutput = "results.txt"

# define grid structure
mutable struct Grid
    points::Array{Float64, 2}
    ax::Float64
    bx::Float64
    ay::Float64
    by::Float64
    Nx::Int
    Ny::Int
end
# default constuctor
Grid() = Grid(Array{Float64, 2}(undef, 0, 0), zeroNumber, zeroNumber, zeroNumber, zeroNumber, noIndex, noIndex)

# define structure to describe physical system
mutable struct System
    grid::Grid
    stiffMatrix::Array{Float64, 2}
    loadVector::Array{Float64, 1}
    corrVector::Array{Float64, 1}
    solVector::Array{Float64, 1}
end
# default constuctor
System() = System(Grid(),  Array{Float64, 2}(undef, 0, 0), Array{Float64, 1}(undef, 0), Array{Float64, 1}(undef, 0),
                  Array{Float64, 1}(undef, 0))

# define functions to calculate mathematical norms
function calculateL0Norm(x1, y1, x2, y2)
    return max(abs(x2 - x1), abs(y2 - y1))
end
function calculateL1Norm(x1, y1, x2, y2)
    return abs(x2 - x1) + abs(y2 - y1)
end
function calculateL2Norm(x1, y1, x2, y2)
    return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
end
