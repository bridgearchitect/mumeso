include("alltypes.jl")

# function to solve system of linear equation numerically (Richardson relaxation)
function solveLinearNum(matrix, solVector, loadVector, omega, numIter)

    # find number of elements
    N = size(solVector, 1)
    # create arrays for current and new solution vectors
    solVectorNew = zeros(N)

    # launch Richardson relaxation
    for k in 1:numIter
        # calculate new solution vector
        solVectorNew = solVector + omega * (loadVector - matrix * solVector)
        for i in 1:N
            # save new solution vector as current
            solVector[i] = solVectorNew[i]
        end
    end

end

# function to solve system of linear equation precisely (Gauss method)
function solveLinearPrec(matrix, loadVector)
   # solve system of linear equations
   return matrix \ loadVector
end
