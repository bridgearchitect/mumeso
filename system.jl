include("alltypes.jl")
include("grid.jl")

# function to determine boundary node in the rectangular lattice
function determineBoundaryNode(i, Nx, Ny)

    # find number of nodes
    numNodes = Nx * Ny
    # check whether this node is boundary
    return (i <= Nx) || (i >= numNodes - Nx + 1) || ((i - 1) % Nx == 0) || (i % Nx == 0)

end

# function to create system
function createSystem(grid, fExt)

    # find number of nodes
    Nx = grid.Nx
    Ny = grid.Ny
    numNodes = Nx * Ny
    # define size of grid cells
    hx = (grid.bx - grid.ax) / (Nx - 1)
    hy = (grid.by - grid.ay) / (Ny - 1)

    # create and fill load vector
    loadVector = Array{Float64, 1}(undef, numNodes)
    for i in 1:numNodes
        # check whether this node is boundary
        if determineBoundaryNode(i, Nx, Ny)
            # zero initialization of load vector (Dirichlet condition)
            loadVector[i] = 0.0
        else
            # default initialization of load vector
            loadVector[i] = fExt
        end
    end

    # create and fill stiffness vector
    stiffMatrix = Array{Float64, 2}(undef, numNodes, numNodes)
    for i in 1:numNodes
        for j in 1:numNodes
            # check whether this node is boundary
            if determineBoundaryNode(i, Nx, Ny)
                # diagonal initialization of stiffness matrix
                if i == j
                    stiffMatrix[i,j] = 1.0
                else
                    stiffMatrix[i,j] = 0.0
                end
            else
                # default initialization of stiffness matrix
                if i == j
                    stiffMatrix[i,j] = 2.0 / (hx * hx) + 2.0 / (hy * hy)
                elseif (i == j + 1) || (i == j - 1)
                    stiffMatrix[i,j] = -1.0 / (hx * hx)
                elseif (i == j + Nx) || (i == j - Nx)
                    stiffMatrix[i,j] = -1.0 / (hy * hy)
                else
                    stiffMatrix[i,j] = 0.0
                end
            end
        end
    end

    # create solution and corrections vectors
    solVector = zeros(Float64, numNodes)
    corVector = zeros(Float64, numNodes)
    # create system object
    system = System(grid, stiffMatrix, loadVector, corVector, solVector)

    # return create system object
    return system

end

# function to restrict one system to the another system
function restrictSystems(systemCoars, systemDen)

    # define number of nodes along x-axis for both systems
    NCoarX = systemCoars.grid.Nx
    NDenX = systemDen.grid.Nx
    # define number of nodes along y-axis for both systems
    NCoarY = systemCoars.grid.Ny
    NDenY = systemDen.grid.Ny
    # define number of nodes for both systems
    numNodesCoars = NCoarX * NCoarY
    numNodesDen = NDenX * NDenY

    # go through all nodes of coarser grid
    for i in 1:numNodesCoars
        # calculate index of denser grid
        iDen = 2 * ((i - 1) % NCoarX) + 2 * NDenX * Int(trunc((i - 1) / NCoarX)) + 1
        # find correction vector for coarser grid
        systemCoars.corrVector[i] = systemDen.solVector[iDen]
    end

end

# function to prolongate one system to another system
function prolongateSystems(systemCoars, systemDen)

    # define number of nodes along x-axis for both systems
    NCoarX = systemCoars.grid.Nx
    NDenX = systemDen.grid.Nx
    # define number of nodes along y-axis for both systems
    NCoarY = systemCoars.grid.Ny
    NDenY = systemDen.grid.Ny
    # define number of nodes for both systems
    numNodesCoars = NCoarX * NCoarY
    numNodesDen = NDenX * NDenY

    # go through all nodes of denser grid
    for i in 1:numNodesDen
        # find coordinates of the current point
        xcur = systemDen.grid.points[i,1]
        ycur = systemDen.grid.points[i,2]
        # find neighbour nodes in grid for current point
        _, neighIndeces = findNeighbourNodesInGrid(systemCoars.grid, xcur, ycur)
        # go through all neighbour nodes
        uaver = 0.0
        numNeigh = size(neighIndeces, 1)
        for j in 1:numNeigh
            # calculate avarage value of solution vector for current points
            uaver += systemCoars.solVector[neighIndeces[j]]
        end
        uaver /= numNeigh
        # update correction vector for denser grid (prolongation)
        systemDen.corrVector[i] = uaver
    end

end

# function to fix error using correction vector
function fixErrorCorrectionVector(system)

    # find number of nodes
    Nx = system.grid.Nx
    Ny = system.grid.Ny
    numNodes = Nx * Ny

    # go through all nodes
    for i in 1:numNodes
        # make correction
        system.solVector[i] = system.solVector[i] + system.corrVector[i]
    end

end
