include("alltypes.jl")
include("grid.jl")
include("system.jl")
include("solver.jl")
include("write.jl")

# create grids
gridCoars = createGrid(NxCoars, NyCoars, axCoars, bxCoars, ayCoars, byCoars)
gridDens = createGrid(NxDens, NyDens, axDens, bxDens, ayDens, byDens)

# create special objects to describe physical systems
systemCoars = createSystem(gridCoars, fExt)
systemDens = createSystem(gridDens, fExt)

# solve physical system using multigrid method
@time begin
    solveSystemsMultigrid(systemCoars, systemDens, numIterMult, numIterRich, omega)
end

# write solution in text file
writeSolution(systemDens, filenameOutput)
